package com.jiusong.gao.uselesslib;

import java.util.Calendar;
import java.util.Date;

public class Useless {

    /**
     * Get formatted time from sending time.
     * @param date
     * @return
     */
    public static String getFormatedTime(Date date) {
        String ret;
        Date currentTime = Calendar.getInstance().getTime();
        long diff = currentTime.getTime() - date.getTime();

        int diffMinutes = (int) (diff / (60 * 1000) % 60);
        int diffHours = (int) (diff / (60 * 60 * 1000) % 24);
        int diffDays = (int) (diff / (24 * 60 * 60 * 1000));

        if (diffDays > 0) {
            ret = diffDays + "d";
        } else if (diffHours > 0) {
            ret = diffHours + "h";
        } else if (diffMinutes > 0) {
            ret = diffMinutes + "m";
        } else {
            ret = "1m";
        }
        return ret;
    }
}
